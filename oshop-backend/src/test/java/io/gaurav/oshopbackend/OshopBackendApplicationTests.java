package io.gaurav.oshopbackend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 
 * @author Gaurav Dighe 03-Nov-2019
 *
 */
@SpringBootTest
class OshopBackendApplicationTests {

	@Test
	void contextLoads() {
	}

}
