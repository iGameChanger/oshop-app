package io.gaurav.oshopbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Gaurav Dighe
 * 03-Nov-2019
 *
 */
@SpringBootApplication
public class OshopBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(OshopBackendApplication.class, args);
	}

}
